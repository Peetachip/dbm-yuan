
 INSERT INTO libraries.books(title, author, publish_date, checked_out, LCCN, book_copy) 
 VALUES ('The relational model for database management', 'E. F. Codd', 1990, false, '89006793', 1),
        ('The C Answer Book', 'Clovis L. Tondo', 1989, false, '88025134', 1),
        ('The C Programming Language, Second Edition', 'Brian  W. Kernighan', 1988, false, '77028983', 1),
        ('The C Programming Language, Second Edition', 'Brian W. Kernighan', 1988, false, '88005934', 2),
        ('The C Programming Language, Second Edition', 'Brian W. Kernighan', 1988, false, '88005934', 3),
        ('The Elements of Artificial Intelligence Using Common Lisp, Second Edition', 'Steven L. Tanimoto', 1995, false, '95003160', 1),
        ('ANSI Common Lisp', 'Paul Graham', 1996, false, '95045017', 1),
        ('Foundations of Analysis', 'Edmund Landau', 1951, false, '51002456', 1)
	;
        
INSERT INTO libraries.patrons(first_name, last_name, patron_id)	
VALUES  ('Peter', 'Yuan', 986220),
	('Chase', 'Dillard', 991212)
	;

INSERT INTO libraries.loans(date_out, date_in, patron_id, LCCN, book_copy)
VALUES	('3/10/19', '', '986220', '89006793', '1')
	;
