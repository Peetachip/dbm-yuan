--Create patrons
DROP TABLE IF EXISTS patrons;
CREATE TABLE patrons(
	first_name	TEXT,
	last_name	TEXT,
	patron_id	TEXT,
	CONSTRAINT users_pk PRIMARY KEY (patron_id)
);

--Create books
DROP TABLE IF EXISTS books;
CREATE TABLE books(
	title		TEXT,
	publish_date	INT,
	LCC		TEXT
	ISBN		TEXT,
	LCCN		INT NOT NULL,
	book_copy	INT NOT NULL,
	CONSTRAINT books_pk PRIMARY KEY (LCCN, book_copy),
	FOREIGN KEY(LCCN, book_copy) REFERENCES books(LCCN, book_copy)
);

--Create loans
DROP TABLE IF EXISTS loans;
CREATE TABLE loans(
	date_out	TEXT,
	date_in		TEXT,
	patron_id_patrons TEXT,
	LCCN		INT,
	book_copy	INT,
        FOREIGN KEY(patron_id_patrons)	REFERENCES patrons(patron_id)
);

ALTER TABLE loans ADD COLUMN loan_uq REFERENCES patrons(partrons_id)
--Connects the pk and fk together
/*
ALTER TABLE loans
ADD patrons_fk FOREIGN KEY(patron_id_patrons)
REFERENCES patrons (patron_id) MATCH FULL
ON DELETE SET NULL ON UPDATE;
*/
/*
ALTER TABLE loans
ADD CONSTRAINT books_fk FOREIGN KEY(LCCN, book_copy)
REFERENCES books (LCCN, book_copy) MATCH FULL
ON DELETE SET NULL ON UPDATE;
*/

