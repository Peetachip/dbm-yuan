DROP TABLE IF EXISTS snakes;
CREATE TABLE snakes(
	color TEXT,
	age	INT,
	scale 	TEXT,
	copy_no	INT
);

INSERT INTO snakes VALUES('green', '2', 'hard', ''),
('blue', '3', 'soft', ''),
('green', '2', 'hard', ''),
('green', '2', 'hard', '');
