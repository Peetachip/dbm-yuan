DROP VIEW IF EXISTS enceladus_events;
CREATE VIEW  enceladus_events AS
SELECT
events.id,
  events.title,
  events.description,
  events.time_stamp,
  events.time_stamp::DATE AS DATE,
  event_types.description AS event,
  to_tsvector(events.description) AS search
FROM events
INNER JOIN event_types
ON event_types.id = events.event_type_id
WHERE target_id=28
ORDER BY time_stamp;


SELECT id, DATE, title
FROM enceladus_events
WHERE DATE
BETWEEN '2005-02-01'::DATE
AND '2005-02-28'::DATE
AND search @@ to_tsquery('thermal');

SELECT title, teams.description
FROM events
INNER JOIN teams ON teams.id=team_id
WHERE time_stamp::DATE='2005-03-09'
AND target_id=28;

SELECT COUNT(1) AS activity, teams.description
FROM events
INNER JOIN teams ON teams.id=team_id
WHERE time_stamp::DATE='2005-03-09'
AND target_id=28;

SELECT COUNT(1) AS activity, teams.description
FROM events
INNER JOIN teams ON teams.id=team_id
WHERE time_stamp::DATE='2005-03-09'
AND target_id=28
GROUP BY teams.description;

SELECT COUNT(1) AS activity, teams.description
FROM events
INNER JOIN teams ON teams.id=team_id
WHERE time_stamp::DATE='2005-03-09'
AND target_id=28
GROUP BY teams.description
ORDER BY activity DESC;
