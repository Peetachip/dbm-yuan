-- Creates a view. A view is a virtual table. 
DROP VIEW IF EXISTS enceladus_events;
CREATE VIEW enceladus_events AS
SELECT 
	events.time_stamp,
	events.time_stamp::DATE AS DATE,
	event_types.description AS EVENT
FROM events
INNER JOIN event_types
	ON event_types.id = events.event_type_id
WHERE target_id=28
ORDER BY time_stamp;

-- This select statement lists your view.
SELECT * FROM enceladus_events
WHERE DATE='2005-02-17';


DROP VIEW IF EXISTS enceladus_events;
CREATE VIEW enceladus_events AS
SELECT 
	events.id,
	events.title,
	events.description,
	events.time_stamp,
	events.time_stamp::DATE AS DATE,
	event_types.description AS event
FROM events
INNER JOIN event_types
	ON event_types.id = events.event_type_id
WHERE target_id=28
ORDER BY time_stamp;

\H
\o feb_2015_flyby.html
SELECT id, time_stamp, title, description FROM enceladus_events WHERE DATE='2005-02-17'::DATE;

