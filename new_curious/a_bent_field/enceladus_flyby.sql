-- First attempt that joins the targets. It lists everything. 
SELECT 
	targets.description AS target,
	events.time_stamp,
	event_types.description AS event
FROM events
INNER JOIN event_types ON event_types.id = events.event_type_id
INNER JOIN targets ON targets.id = events.target_id
WHERE events.time_stamp::DATE='2005-02-17'
ORDER BY events.time_stamp;

-- Second attempt that joins the targets and tries to specify the target as ecneladus. This one brings up no responses because it is too specific.
SELECT
        targets.description AS target,
        events.time_stamp,
        event_types.description AS event
FROM events
INNER JOIN event_types ON event_types.id = events.event_type_id
INNER JOIN targets ON targets.id = events.target_id
WHERE events.time_stamp::DATE='2005-02-17'
AND targets.description = 'enceladus'
ORDER BY events.time_stamp;

-- Third attempt that joins the targets, but the 'Enceladus' id was used to focus the results.
SELECT
        targets.description AS target,
        events.time_stamp,
        event_types.description AS event
FROM events
INNER JOIN event_types ON event_types.id = events.event_type_id
INNER JOIN targets ON targets.id = events.target_id
WHERE events.time_stamp::DATE='2005-02-17'
AND targets.id = 28
ORDER BY events.time_stamp;

