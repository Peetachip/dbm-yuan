SELECT id, title FROM enceladus_events
WHERE search @@ to_tsquery('closest');

DROP VIEW IF EXISTS enceladus_events;
CREATE VIEW enceladus_events AS
SELECT
  events.id,
  events.title,
  events.description,
  events.time_stamp,
  events.time_stamp::DATE AS DATE,
  event_types.description AS event,
  to_tsvector(
	concat(events.description,' ',events.title)
) AS search
FROM events
INNER JOIN event_types
ON event_types.id = events.event_type_id
WHERE target_id=28
ORDER BY time_stamp;
