DROP TABLE events;
CREATE TABLE events (
    id                  SERIAL PRIMARY KEY,
    time_stamp          TIMESTAMPTZ NOT NULL,
    title               VARCHAR(500),
    description         TEXT,
    event_type_id       INT,
    spass_type_id       INT,
    target_id           INT,
    team_id             INT,
    request_id          INT
);

INSERT INTO events(
	time_stamp,
	title,
	description
)
SELECT
	import.master_plan.start_time_utc::TIMESTAMP,
	import.master_plan.title,
	import.master_plan.description
FROM import.master_plan;

